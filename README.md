In order to build/install the MushTracker project, follow the instructions below.

1. Install Android Studio.
2. Open the MushTracker folder in Android Studio
3. Click the build or green 'play' button to build the project
4. Run the emulator

When the project is running

1. Use the camera, geolocation and date features in order to track a mushroom finding
2. Add this finding to your log. 
3. Review your logs on the mushroomLog tab

A LICENSE has been created for this project. I have created a license for this as because this repository is public, I am treating it as 'open source'. This way it gives those that work on the project free range to copy, edit, sell etc.
If the repository was private, licensing would be done differently. Only those involved in the code would be included in the product rights, and even then if the project is being done for a client, is is likely that they would like full 
property rights to the product. 

This is a change I am making to this file.